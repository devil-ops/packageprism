package prism

import (
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.oit.duke.edu/devil-ops/packageprism/indexers/apt"
	"gitlab.oit.duke.edu/devil-ops/packageprism/indexers/asset"
	"gopkg.in/yaml.v3"
)

func init() {
	slog.SetDefault(slog.New(slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{
		Level: slog.LevelDebug,
	})))
}

func TestNewServer(t *testing.T) {
	srv := MustNew(
		WithAdminToken("test"),
	)
	require.NotNil(t, srv)
	srv.addRepo(newYumRepoType(srv, "my-yum-repo"))
	srv.addRepo(newAptRepoType(srv, "my-apt-repo"))

	go srv.Run()
	// Give it a sec to spin up
	time.Sleep(time.Second * 1)

	// Now test with a good token
	assert.Contains(t,
		testReqDo("GET", fmt.Sprintf("http://localhost:%v/api/v1/repos", srv.Port()), nil),
		`"id":"my-apt-repo","indexer":{"type":"apt"`,
	)
	assert.Contains(t,
		testReqDo("GET", fmt.Sprintf("http://localhost:%v/api/v1/repos/my-apt-repo", srv.Port()), nil),
		`"id":"my-apt-repo","indexer":{"type":"apt"`,
	)
	assert.Equal(t,
		`{"errors":["no repo found with id: never-exists"]}`,
		testReqDo("GET", fmt.Sprintf("http://localhost:%v/api/v1/repos/never-exists", srv.Port()), nil),
	)
}

/*
func TestIndexYum(t *testing.T) {
	d := t.TempDir()
	srv := MustNew(
		WithStoragePath(d),
	)
	r := newYumRepoType(srv, "test-yum-index")
	require.NoError(t, srv.addRepo(r))
	require.NoError(t, srv.index(*r))
	require.FileExists(t, path.Join(d, r.ID, "repodata/repomd.xml"))
}
*/

func TestWithConfig(t *testing.T) {
	d := t.TempDir()
	srv := MustNew(WithConfig(Config{
		StoragePath: d,
		Repos: RepoList{
			{
				ID:      "test-apt",
				Indexer: &apt.Indexer{},
			},
			{
				ID:      "test-asset",
				Indexer: asset.New(""),
			},
		},
	}))
	require.NotNil(t, srv)
	require.NoError(t, srv.addRepo(
		newYumRepoType(srv, "some-yum"),
	))
	got, err := srv.getRepo("some-yum")
	require.NoError(t, err)
	require.NotNil(t, got)
}

func TestAddPackage(t *testing.T) {
	srv := MustNew(
		WithAdminToken("test"),
	)
	require.NotNil(t, srv)
	require.NoError(t, srv.addRepo(newYumRepoType(srv, "yum-repo")))

	go srv.Run()
	// Give it a sec to spin up
	time.Sleep(time.Second * 1)

	f, err := os.Open("testdata/packages/helloworld-1.0.0-1.x86_64.rpm")
	require.NoError(t, err)
	defer dclose(f)
	req, err := http.NewRequest("POST", fmt.Sprintf("http://localhost:%v/api/v1/repos/yum-repo/packages/package.rpm", srv.Port()), f)
	require.NoError(t, err)

	req.Header.Add("Authorization", "Bearer test")
	res, err := http.DefaultClient.Do(req)
	require.NoError(t, err)
	defer dclose(res.Body)
	got, err := io.ReadAll(res.Body)
	require.NoError(t, err)

	// Now test with a good token
	assert.Equal(t,
		"OK",
		string(got),
	)
}

func TestMarshalRepoConfig(t *testing.T) {
	given := []byte(`
id: "test"
indexer:
 type: yum
 path: /foo/bar
`)

	var target Repo
	require.NoError(t, yaml.Unmarshal(given, &target))
}

func TestMarshalRepoListConfig(t *testing.T) {
	given := []byte(`
- id: "test"
  indexer:
   type: yum
   path: /foo/bar
`)

	var target RepoList
	require.NoError(t, yaml.Unmarshal(given, &target))
}

func TestAddPackageMissingRepo(t *testing.T) {
	srv := MustNew(
		WithAdminToken("test"),
	)
	require.NotNil(t, srv)
	require.NoError(t, srv.addRepo(newYumRepoType(srv, "yum-repo")))

	go srv.Run()
	// Give it a sec to spin up
	time.Sleep(time.Second * 1)

	f, err := os.Open("testdata/packages/helloworld-1.0.0-1.x86_64.rpm")
	require.NoError(t, err)
	defer dclose(f)
	req, err := http.NewRequest("POST", fmt.Sprintf("http://localhost:%v/api/v1/repos/this-repo-never-exists/packages/package.rpm", srv.Port()), f)
	require.NoError(t, err)

	req.Header.Add("Authorization", "Bearer test")
	res, err := http.DefaultClient.Do(req)
	require.NoError(t, err)
	defer dclose(res.Body)
	got, err := io.ReadAll(res.Body)
	require.NoError(t, err)

	// Now test with a good token
	assert.Equal(t, "404 page not found\n", string(got))
}

func TestRepoExist(t *testing.T) {
	d := t.TempDir()
	srv := MustNew(
		WithStoragePath(d),
		WithAdminToken("test"),
	)
	require.NotNil(t, srv)

	r := newYumRepoType(srv, "test-repo-get")
	srv.addRepo(newYumRepoType(srv, r.ID))

	assert.True(t, srv.repoExists(r.ID))
	assert.False(t, srv.repoExists("test-repo-missing"))

	assert.DirExists(t, r.fsPath())
}

func TestRepoStorage(t *testing.T) {
	srv := MustNew(WithAdminToken("test"))
	r := newAptRepoType(srv, "test-repo-storage")
	require.NoError(t, srv.addRepo(r))
	require.NoError(t, r.commit())

	got, err := srv.getRepo("test-repo-storage")
	require.NoError(t, err)
	require.NotNil(t, got)
}

func TestPublicRepo(t *testing.T) {
	srv := MustNew(
		WithStoragePath(t.TempDir()),
		WithAdminToken("test"),
	)
	go srv.Run()
	time.Sleep(1 * time.Second)

	r := newYumRepoType(srv, "test-public-repo")
	srv.addRepo(newYumRepoType(srv, r.ID))

	require.Equal(t,
		"<pre>\n<a href=\"repodata/\">repodata/</a>\n</pre>\n",
		testGetReqDo(fmt.Sprintf("http://localhost:%v/public/test-public-repo/", srv.Port())),
		"Make sure the public endpoint is actually public",
	)

	require.Equal(t,
		"<pre>\n<a href=\"repodata/\">repodata/</a>\n</pre>\n",
		testGetReqDo(fmt.Sprintf("http://localhost:%v/public/test-public-repo", srv.Port())),
		"Make sure the public endpoint is actually public, even without the trailing /",
	)
}
