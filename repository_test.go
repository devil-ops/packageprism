package prism

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.oit.duke.edu/devil-ops/packageprism/indexers/yum"
	"gopkg.in/yaml.v3"
)

func TestRepositoryPath(t *testing.T) {
	require.Equal(t,
		[]byte("repositories/foo"),
		Repo{
			ID:      "foo",
			Indexer: yum.New(""),
		}.dbPath(),
	)
}

func TestRepositoryMarshal(t *testing.T) {
	// Test yum
	yum := Repo{
		ID:      "test yum marshal",
		Indexer: yum.New("/foo/bar"),
	}
	got, err := json.Marshal(yum)
	require.NoError(t, err)
	assert.Equal(
		t,
		"{\"id\":\"test yum marshal\",\"indexer\":{\"type\":\"yum\",\"path\":\"/foo/bar\"}}",
		string(got),
	)
}

func TestRepositoryUnmarshal(t *testing.T) {
	var got Repo
	assert.NoError(t, json.Unmarshal([]byte(`{"id":"my-apt-repo","indexer":{"type":"yum"}}`), &got))
	assert.NoError(t, json.Unmarshal([]byte(`{"id":"my-asset-repo","indexer":{"type":"asset"}}`), &got))

	var gots RepoList
	assert.NoError(t, json.Unmarshal([]byte(`[{"id":"my-yum-repo","indexer":{"type":"yum"}},{"id":"my-other-yum-repo","indexer":{"type":"yum"}}]`), &gots))
}

func TestExtractRepo(t *testing.T) {
	tests := map[string]struct {
		given     map[string]any
		expectErr string
	}{
		"good-asset": {
			given: map[string]any{
				"id": "test-assets",
				"indexer": map[string]any{
					"type": "asset",
				},
			},
		},
		"missing-type-asset": {
			given: map[string]any{
				"id": "test-assets",
				"indexer": map[string]any{
					"type": "",
				},
			},
			expectErr: "empty type field in indexer",
		},
		"invalid-type-asset": {
			given: map[string]any{
				"id": "test-assets",
				"indexer": map[string]any{
					"type": "never-exists",
				},
			},
			expectErr: "unknown index mapper: never-exists",
		},
		"good-apt": {
			given: map[string]any{
				"id": "test-extracts",
				"indexer": map[string]any{
					"type": "apt",
				},
			},
		},
		"good-yum": {
			given: map[string]any{
				"id": "test-extracts",
				"indexer": map[string]any{
					"type": "yum",
				},
			},
		},
		"missing-indexer": {
			given:     map[string]any{},
			expectErr: "missing 'indexer' field",
		},
		"empty-indexer": {
			given: map[string]any{
				"indexer": map[string]any{
					"type": "",
				},
			},
			expectErr: "empty type field in indexer",
		},
		"invalid-indexer": {
			given: map[string]any{
				"indexer": map[string]any{
					"type": "never-exists",
				},
			},
			expectErr: "unknown index mapper: never-exists",
		},
	}

	for desc, tt := range tests {
		_, err := extractIndexer(tt.given)
		if tt.expectErr != "" {
			assert.EqualError(t, err, tt.expectErr, desc)
		} else {
			assert.NoError(t, err, desc)
		}
	}
}

func TestYAMLUnmarshal(t *testing.T) {
	var r Repo
	assert.NoError(t, yaml.Unmarshal([]byte(`---
id: "foo"
indexer:
  type: asset
`), &r))
}

func TestExtractIndexerWithFields(t *testing.T) {
	got, err := extractIndexer(
		map[string]any{
			"id": "test-assets",
			"indexer": map[string]any{
				"type": "asset",
				"path": "/tmp/foo/bar",
			},
		})
	require.NoError(t, err)
	gotB, err := json.Marshal(got)
	require.NoError(t, err)
	require.Equal(t,
		`{"type":"asset","path":"/tmp/foo/bar"}`,
		string(gotB),
	)
}
