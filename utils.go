package prism

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"path"
	"strings"

	"gitlab.oit.duke.edu/devil-ops/packageprism/indexers/apt"
	"gitlab.oit.duke.edu/devil-ops/packageprism/indexers/yum"
)

func dclose(c io.Closer) {
	if err := c.Close(); err != nil {
		fmt.Fprintf(os.Stderr, "error closing item")
	}
}

func newAptRepoType(s *Server, id string) *Repo {
	r := Repo{
		ID:     id,
		server: s,
	}
	r.Indexer = apt.New(
		path.Join(s.storagePath, id),
	)
	return &r
}

func newYumRepoType(s *Server, id string) *Repo {
	return &Repo{
		ID:      id,
		Indexer: yum.New(path.Join(s.storagePath, id)),
		server:  s,
	}
}

type logWriter struct {
	http.ResponseWriter
	statusCode int
}

func newLogWriter(w http.ResponseWriter) *logWriter {
	// WriteHeader(int) is not called if our response implicitly returns 200 OK, so
	// we default to that status code.
	return &logWriter{ResponseWriter: w, statusCode: http.StatusOK}
}

func (lrw *logWriter) WriteHeader(code int) {
	lrw.statusCode = code
	lrw.ResponseWriter.WriteHeader(code)
}

func trimQuotes(s string) string {
	return strings.TrimSuffix(strings.TrimPrefix(s, `"`), `"`)
}
