package prism

import (
	"io"
	"net/http"
)

func testReq(method, url string, body io.Reader) *http.Request {
	req, err := http.NewRequest(method, url, body)
	panicIfErr(err)
	req.Header.Add("Authorization", "Bearer test")
	return req
}

func testReqDo(method, url string, body io.Reader) string {
	req := testReq(method, url, body)
	res, err := http.DefaultClient.Do(req)
	panicIfErr(err)
	defer res.Body.Close()
	data, err := io.ReadAll(res.Body)
	panicIfErr(err)
	return string(data)
}

func testGetReqDo(url string) string {
	req := testReq("GET", url, nil)
	res, err := http.DefaultClient.Do(req)
	panicIfErr(err)
	defer res.Body.Close()
	data, err := io.ReadAll(res.Body)
	panicIfErr(err)
	return string(data)
}
