#!/usr/bin/env bash
set -e
set -x

BASEURL="http://localhost:8090"

curl -X POST ${BASEURL}/api/v1/repos/test-yum/packages/ --header "Authorization: Bearer test" --upload-file testdata/packages/helloworld-1.0.0-1.x86_64.rpm
curl -X POST ${BASEURL}/api/v1/repos/test-asset/packages/ --header "Authorization: Bearer test" --upload-file testdata/packages/helloworld_1.0.0_amd64.deb
curl -X POST ${BASEURL}/api/v1/repos/test-apt/packages/ --header "Authorization: Bearer test" --upload-file testdata/packages/helloworld_1.0.0_amd64.deb