/*
Package prism does all of the package prisming we need! 💎
*/
package prism

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"log/slog"
	"net"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"time"

	"github.com/google/uuid"
	"gitlab.oit.duke.edu/devil-ops/packageprism/db"
	"gitlab.oit.duke.edu/devil-ops/packageprism/db/builtin"
	"gitlab.oit.duke.edu/devil-ops/packageprism/indexers"
)

// MaxUploadSize sets a maximum size for the uploads to the server
// const MaxUploadSize = 1024 * 1024 * 1024

// Server holds on the stuff that serves up a travel agent
type Server struct {
	listener       net.Listener
	listenAddr     string
	adminToken     string
	bootstrapRepos RepoList
	storagePath    string
	tmpPath        string
	DB             db.DB
	dirMode        fs.FileMode
	fileMode       fs.FileMode
	mux            *http.ServeMux
	// dbf        string
}

// Config represents the configuration of a server
type Config struct {
	StoragePath string   `yaml:"storage_path,omitempty"`
	Repos       RepoList `yaml:"repos,omitempty"`
	AdminToken  string   `yaml:"admin_token,omitempty"`
	Addr        string   `yaml:"addr,omitempty"`
}

// WithConfig sets things from a Config object on a new server
func WithConfig(c Config) func(*Server) {
	return func(s *Server) {
		if c.StoragePath != "" {
			s.storagePath = c.StoragePath
		}
		s.bootstrapRepos = append(s.bootstrapRepos, c.Repos...)
		if c.AdminToken != "" {
			s.adminToken = c.AdminToken
		}
		if c.Addr != "" {
			s.listenAddr = c.Addr
		}
	}
}

// WithStoragePath sets the base storage path for a server
func WithStoragePath(p string) func(*Server) {
	return func(s *Server) {
		s.storagePath = p
	}
}

// WithRepos sets the repository listing for a given server at startup
func WithRepos(r ...Repo) func(*Server) {
	return func(s *Server) {
		s.bootstrapRepos = r
	}
}

// WithAdminToken sets the admin token for a server
func WithAdminToken(t string) func(*Server) {
	return func(s *Server) {
		s.adminToken = t
	}
}

// WithListener sets the listener on a server object
func WithListener(l net.Listener) func(*Server) {
	return func(s *Server) {
		s.listener = l
	}
}

func (s *Server) initAdminToken() {
	// Set an random admin token if not given
	if s.adminToken == "" {
		guuid := fmt.Sprint(uuid.New())
		slog.Warn("setting a random admin token", "token", guuid)
		s.adminToken = guuid
	}
}

func (s *Server) setListener() error {
	if s.listener == nil {
		defaultListener, err := net.Listen("tcp", s.listenAddr)
		if err != nil {
			return err
		}
		s.listener = defaultListener
	}
	return nil
}

func (s *Server) initStorage() error {
	// Use temp directory if not given
	if s.storagePath == "" {
		d, err := os.MkdirTemp("", "prism")
		if err != nil {
			return err
		}
		s.storagePath = d
	}
	return nil
}

func (s *Server) initTmp() error {
	// Set up a temp directory under the storage
	if s.tmpPath == "" {
		s.tmpPath = path.Join(s.storagePath, "temp")
		if err := s.mkdir(s.tmpPath); err != nil {
			return err
		}
	}
	return nil
}

func (s *Server) initBootStrapRepos() error {
	// Bootstrap repos if they exist
	for _, prepo := range s.bootstrapRepos {
		slog.Debug("bootstrapping repo", "id", prepo.ID)
		r := &Repo{
			ID:     prepo.ID,
			server: s,
		}
		if !s.repoExists(prepo.ID) {
			c, err := indexers.Create(prepo.Indexer.String(), path.Join(s.storagePath, prepo.ID))
			if err != nil {
				return err
			}
			r.Indexer = c
			if err := s.addRepo(r); err != nil {
				return err
			}
		} else {
			slog.Info("bootstrap repo already exists", "repo", prepo.ID)
		}
	}
	s.bootstrapRepos = nil
	slog.Debug("completed bootstraping repos")
	return nil
}

// MustNew returns a new server or panics on error
func MustNew(opts ...func(*Server)) *Server {
	got, err := New(opts...)
	if err != nil {
		panic(err)
	}
	return got
}

// New returns a new server with functional options and optional error
func New(opts ...func(*Server)) (*Server, error) {
	s := &Server{
		listenAddr: "127.0.0.1:0",
		dirMode:    0o700,
		fileMode:   0o600,
	}
	for _, opt := range opts {
		opt(s)
	}

	s.initAdminToken()
	for _, initr := range []func() error{
		s.setListener,
		s.initStorage,
		s.initTmp,
		s.initDB,
		s.initBootStrapRepos,
	} {
		if err := initr(); err != nil {
			return nil, err
		}
	}
	// Routing and muxing and such...
	s.mux = http.NewServeMux()
	addRoutes(s, s.mux)

	return s, nil
}

var repoBucket = []byte("repositories")

// initDB initializes the database
func (s *Server) initDB() error {
	var err error
	s.DB, err = builtin.New(s.storagePath)
	return err
}

func (s *Server) allRepos() (RepoList, error) {
	var ret RepoList
	if err := s.DB.GetAll(db.RepoBucket, "", &ret); err != nil {
		return nil, err
	}
	return ret, nil
}

func (s *Server) mustAllRepos() RepoList {
	got, err := s.allRepos()
	if err != nil {
		panic(err)
	}
	return got
}

func (s *Server) getRepo(id string) (*Repo, error) {
	if id == "" {
		return nil, errors.New("id cannot be empty")
	}
	var ret Repo
	if err := s.DB.Get(db.RepoBucket, id, &ret); err != nil {
		return nil, err
	}
	ret.server = s

	if err := ret.Indexer.Init(s.DB); err != nil {
		return nil, err
	}

	return &ret, nil
}

func (s *Server) repoExists(id string) bool {
	_, err := s.getRepo(id)
	return err == nil
}

func (s *Server) addRepo(repos ...*Repo) error {
	for _, item := range repos {
		slog.Info("adding repo", "repo", item.ID, "indexer", item.Indexer)
		if err := s.mkdir(item.fsPath()); err != nil {
			return err
		}

		// Initialize the repo when added. This will make tools like yum and apt
		// behave before we have published any packages
		if err := item.Indexer.Init(s.DB); err != nil {
			return err
		}

		// It seems we shouldn't need this additional commit since the Init()
		// above commits as well...but things break...
		slog.Info("About to commit after adding repo", "repo", item.ID)
		if err := item.commit(); err != nil {
			return err
		}
	}
	return nil
}

type middleware func(h http.HandlerFunc) http.HandlerFunc

func multipleMiddleware(h http.HandlerFunc, m ...middleware) http.HandlerFunc {
	if len(m) < 1 {
		return h
	}

	wrapped := h

	// loop in reverse to preserve middleware order
	for i := len(m) - 1; i >= 0; i-- {
		wrapped = m[i](wrapped)
	}

	return wrapped
}

func (s *Server) handleStaticRepo(w http.ResponseWriter, r *http.Request) {
	id := r.PathValue("repo_id")
	lpath, err := filepath.Abs(path.Join(s.storagePath, id))
	panicIfErr(err)
	h := http.StripPrefix(
		path.Join("/public/", id),
		http.FileServer(http.Dir(lpath)),
	)
	h.ServeHTTP(w, r)
}

// Run runs a given  router and starts the HTTP server
func (s *Server) Run() error {
	hs := &http.Server{
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		Handler:      s.mux,
	}
	return hs.Serve(s.listener)
}

// Port returns the port that a server is listening on
func (s *Server) Port() int {
	return s.listener.Addr().(*net.TCPAddr).Port
}

// Addr returns the listening address
func (s Server) Addr() net.Addr {
	return s.listener.Addr()
}

func (s Server) validateToken(w http.ResponseWriter, r *http.Request) error {
	if r.Header.Get("Authorization") != fmt.Sprintf("Bearer %v", s.adminToken) {
		w.WriteHeader(401)
		resB, merr := json.Marshal(ErrorResponse{
			Errors: []string{"invalid token"},
		})
		if merr != nil {
			return merr
		}
		if _, werr := w.Write(resB); werr != nil {
			return werr
		}
		return errors.New("invalid token")
	}
	return nil
}

func panicIfErr(err error) {
	if err != nil {
		panic(err)
	}
}

// ErrorResponse represents an error from the api
type ErrorResponse struct {
	Errors []string `json:"errors"`
}

func (s *Server) handleGetRepo(w http.ResponseWriter, r *http.Request) {
	if err := s.validateToken(w, r); err != nil {
		return
	}
	id := r.PathValue("repo_id")
	repo, err := s.getRepo(id)
	if err != nil {
		w.WriteHeader(404)
		mustWrite(w, mustMarshal(ErrorResponse{
			Errors: []string{fmt.Sprintf("no repo found with id: %v", id)},
		}))
		return
	}
	w.WriteHeader(http.StatusOK)
	mustWrite(w, mustMarshal(repo))
}

func mustWrite(w http.ResponseWriter, v []byte) {
	_, err := w.Write(v)
	if err != nil {
		slog.Warn("error writing response", "error", err)
	}
}

func mustMarshal(v any) []byte {
	got, err := json.Marshal(v)
	if err != nil {
		slog.Error("error unmarshaling", "item", v, "error", err)
		panic(err)
	}
	return got
}

func (s *Server) handleListRepos(w http.ResponseWriter, r *http.Request) {
	if err := s.validateToken(w, r); err != nil {
		return
	}

	repos := mustMarshal(s.mustAllRepos())
	w.WriteHeader(http.StatusOK)
	mustWrite(w, repos)
}

func (s *Server) handleAddPackage(w http.ResponseWriter, r *http.Request) {
	slog.Info("Attempting to add a file")
	if err := s.validateToken(w, r); err != nil {
		return
	}
	name := r.PathValue("package_name")
	id := r.PathValue("repo_id")
	td := path.Join(s.tmpPath, id) // temp repo path
	panicIfErr(s.mkdir(td))
	tfile := path.Join(td, name)
	f, err := os.Create(path.Clean(tfile))
	panicIfErr(err)

	_, err = io.Copy(f, r.Body)
	panicIfErr(err)

	repo, err := s.getRepo(id)
	if err != nil {
		http.NotFound(w, r)
		return
	}

	slog.Debug("Got repo out of the db", "indexer", repo.Indexer)

	if err := repo.Indexer.Add(tfile); err != nil {
		w.WriteHeader(http.StatusBadGateway)
		mustWrite(w, []byte(err.Error()))
		return
	}

	slog.Info("Successfully added file", "file", tfile)
	mustWrite(w, []byte("OK"))
}

func (s Server) mkdir(d string) error {
	if _, err := os.Stat(d); os.IsNotExist(err) {
		slog.Info("Creating directory", "path", d)
		return os.MkdirAll(d, s.dirMode)
	}
	return nil
}
