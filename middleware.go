package prism

import (
	"encoding/json"
	"fmt"
	"log/slog"
	"net/http"
	"time"
)

func (s Server) validateTokenX(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			if r.Header.Get("Authorization") != fmt.Sprintf("Bearer %v", s.adminToken) {
				slog.Warn("Invalid authorization")
				w.WriteHeader(401)
				resB, merr := json.Marshal(ErrorResponse{
					Errors: []string{"invalid token"},
				})
				panicIfErr(merr)
				_, werr := w.Write(resB)
				panicIfErr(werr)
			}
			next.ServeHTTP(w, r)
		})
}

func handleLogsX(h http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(
		func(w http.ResponseWriter, req *http.Request) {
			start := time.Now()
			lrw := newLogWriter(w)
			h.ServeHTTP(lrw, req)

			slog.Info("access",
				"method", req.Method,
				"url", req.URL.Path,
				"code", lrw.statusCode,
				"duration", time.Since(start),
				"remote", req.RemoteAddr,
				"agent", req.UserAgent(),
			)
		})
}
