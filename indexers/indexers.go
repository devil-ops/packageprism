/*
Package indexers describes how the packages should be indexed. This includes things like yum or apt metadata
*/
package indexers

import (
	"errors"
	"fmt"
	"sort"

	"gitlab.oit.duke.edu/devil-ops/packageprism/db"
	"gitlab.oit.duke.edu/devil-ops/packageprism/indexers/apt"
	"gitlab.oit.duke.edu/devil-ops/packageprism/indexers/asset"
	"gitlab.oit.duke.edu/devil-ops/packageprism/indexers/yum"
)

const (
	// YUM is the string representation of a Yum indexer
	YUM = "yum"
	// APT is the string representation of a Apt indexer
	APT = "apt"
	// ASSET is the string representation of a Asset indexer
	ASSET = "asset"
)

// Creator is a generator for indexers using a string as the path input argument
type Creator func(string) Indexer

// CreatorWithMap is a generator for indexers using a map as the input argument
type CreatorWithMap func(map[string]any) (Indexer, error)

// Indexers defines a map of labels to Creators
var Indexers = map[string]Creator{
	YUM:   func(s string) Indexer { return yum.New(s) },
	APT:   func(s string) Indexer { return apt.New(s) },
	ASSET: func(s string) Indexer { return asset.New(s) },
}

// IndexersWithMap defines a map of labels to Creators using a map as the input
var IndexersWithMap = map[string]CreatorWithMap{
	YUM:   func(m map[string]any) (Indexer, error) { return yum.NewWithMap(m) },
	APT:   func(m map[string]any) (Indexer, error) { return apt.NewWithMap(m) },
	ASSET: func(m map[string]any) (Indexer, error) { return asset.NewWithMap(m) },
}

// Create brings up a new indexer of a certain type
func Create(s, p string) (Indexer, error) {
	if creator, ok := Indexers[s]; ok {
		return creator(p), nil
	}
	return nil, fmt.Errorf("unknown indexer: %s", s)
}

// CreateWithMap brings up a new indexer of a certain type using a map as input
func CreateWithMap(s string, m map[string]any) (Indexer, error) {
	if s == "" {
		return nil, errors.New("empty type field in indexer")
	}
	if creator, ok := IndexersWithMap[s]; ok {
		return creator(m)
	}
	return nil, fmt.Errorf("unknown index mapper: %s", s)
}

// MustCreate returns a new indexer or panics on error
func MustCreate(s, p string) Indexer {
	got, err := Create(s, p)
	if err != nil {
		panic(err)
	}
	return got
}

/*
This lands us in a dependency loop, not sure how we would use it in init properly, if at all
// Add a new format creator
func Add(name string, creator Creator) {
	Indexers[name] = creator
}
*/

// Names returns a slice of the names of the indexers
func Names() []string {
	ret := make([]string, 0, len(Indexers))
	for k := range Indexers {
		ret = append(ret, k)
	}
	sort.Strings(ret)
	return ret
}

// Indexer is a the interface that describes how a Repo gets indexed
type Indexer interface {
	// Init does the work to make sure indexing will run, including setting various operators like the db
	Init(db.DB) error
	// String just returns the string representation of the Indexer
	String() string
	// Add takes a path, generally the temp file that was just uploaded, and adds it to the repository via this indexer
	Add(string) error
}
