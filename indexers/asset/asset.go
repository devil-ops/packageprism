/*
Package asset operates on static file assets. This is the simplest of all use cases
*/
package asset

import (
	"errors"
	"log/slog"
	"os"
	"path"

	"gitlab.oit.duke.edu/devil-ops/packageprism/db"
)

// Indexer is the main thing that understands how to deal with 'Assets'
type Indexer struct {
	Type string `json:"type"`
	Path string `json:"path"`
}

// New constructs a new Asset Indexer with the given base path
func New(s string) *Indexer {
	return &Indexer{
		Type: "asset",
		Path: s,
	}
}

// Add adds new packages to the asset repository
func (o Indexer) Add(source string) error {
	if o.Path == "" {
		return errors.New("no path set on indexer yet")
	}
	fn := path.Base(source)
	slog.Debug("About to create path", "path", o.Path, "filename", fn)
	if err := os.MkdirAll(o.Path, 0o700); err != nil {
		return err
	}
	dest := path.Join(o.Path, fn)
	if err := os.Rename(source, dest); err != nil {
		return err
	}
	return nil
}

// String returns the string representation
func (o Indexer) String() string {
	return "asset"
}

// Init initializes a new Asset repo
func (o *Indexer) Init(_ db.DB) error {
	if o.Path == "" {
		return errors.New("must set Path before asset.Init()")
	}
	return os.MkdirAll(o.Path, 0o700)
}

// NewWithMap returns a new Asset indexer from a map[string]any
func NewWithMap(m map[string]any) (*Indexer, error) {
	var path string
	if pathI, ok := m["path"]; ok {
		if path, ok = pathI.(string); !ok {
			return nil, errors.New("missing 'path' on asset indexer")
		}
	}
	return New(path), nil
}
