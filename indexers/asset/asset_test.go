package asset

import (
	"encoding/json"
	"os"
	"path"
	"testing"

	"github.com/stretchr/testify/require"
	"gopkg.in/yaml.v3"
)

func TestString(t *testing.T) {
	require.Equal(t, "asset", Indexer{}.String())
}

func TestAdd(t *testing.T) {
	tmpF := path.Join(t.TempDir(), "some-file.txt")
	require.NoError(t, os.WriteFile(tmpF, []byte("hello"), 0o600))

	tpath := t.TempDir()
	o := Indexer{Path: tpath}
	require.NoError(t, o.Add(tmpF))
	require.FileExists(t, path.Join(tpath, "some-file.txt"))
}

func TestInit(t *testing.T) {
	o := New("")
	require.EqualError(t,
		o.Init(nil),
		"must set Path before asset.Init()",
	)

	p := path.Join(t.TempDir(), "some-dir")
	o = New(p)
	require.NoError(t, o.Init(nil))
	require.DirExists(t, p)
}

func TestMarshal(t *testing.T) {
	o := New("/foo/bar")
	got, err := json.Marshal(o)
	// got, err := o.MarshalJSON()
	require.NoError(t, err)
	require.Equal(t,
		"{\"type\":\"asset\",\"path\":\"/foo/bar\"}",
		string(got),
	)
}

func TestUnmarshalJSON(t *testing.T) {
	var target Indexer
	require.NoError(t, json.Unmarshal([]byte(`{"type":"asset","path":"/tmp/prism/test-asset"}`), &target))
	require.Equal(t,
		New("/tmp/prism/test-asset"),
		&target,
	)
}

func TestUnmarshalYAML(t *testing.T) {
	var target Indexer
	require.NoError(t, yaml.Unmarshal([]byte(`---
type: asset
path: /tmp/prism/test-asset
`), &target))
	require.Equal(t,
		New("/tmp/prism/test-asset"),
		&target,
	)
}

func TestNewWithMap(t *testing.T) {
	got, err := NewWithMap(map[string]any{
		"type": "asset",
		"path": "/foo/bar",
	})
	require.NoError(t, err)
	require.Equal(t,
		New("/foo/bar"),
		got,
	)
}
