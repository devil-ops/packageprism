package yum

import (
	"encoding/json"
	"io"
	"log/slog"
	"os"
	"path"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestString(t *testing.T) {
	require.Equal(t, "yum", Indexer{}.String())
}

func TestInit(t *testing.T) {
	o := New("")
	require.EqualError(t,
		o.Init(nil),
		"must set Path before yum.Init()",
	)

	p := path.Join(t.TempDir(), "some-dir")
	o = New(p)
	require.NoError(t, o.Init(nil))
	require.DirExists(t, p)
}

func TestAdd(t *testing.T) {
	tmpF := path.Join(t.TempDir(), "helloworld-1.0.0-1.x86_64.rpm")
	require.NoError(t, copyFile("../../testdata/packages/helloworld-1.0.0-1.x86_64.rpm", tmpF))

	tpath := t.TempDir()

	o := Indexer{Path: tpath}
	require.NoError(t, o.Add(tmpF))
	require.FileExists(t, path.Join(tpath, "Packages/h/helloworld-1.0.0-1.x86_64.rpm"))
}

func TestMarshal(t *testing.T) {
	o := New("/foo/bar")
	got, err := json.Marshal(o)
	require.NoError(t, err)
	require.Equal(t,
		"{\"type\":\"yum\",\"path\":\"/foo/bar\"}",
		string(got),
	)
}

func copyFile(in, out string) error {
	i, e := os.Open(path.Clean(in))
	if e != nil {
		return e
	}
	defer dclose(i)
	o, e := os.Create(path.Clean(out))
	if e != nil {
		return e
	}
	defer dclose(o)
	_, err := o.ReadFrom(i)
	return err
}

func dclose(c io.Closer) {
	if err := c.Close(); err != nil {
		slog.Error("error closing item")
	}
}
