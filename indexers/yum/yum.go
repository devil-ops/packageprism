/*
Package yum handles how to work with Yum package repositories
*/
package yum

import (
	"errors"
	"log/slog"
	"os"
	"path"

	"github.com/stianwa/createrepo"
	"gitlab.oit.duke.edu/devil-ops/packageprism/db"
)

// New returns a new Yum Indexer
func New(s string) *Indexer {
	return &Indexer{
		Type: "yum",
		Path: s,
	}
}

// NewWithMap returns a new Yum indexer from a map[string]any
func NewWithMap(m map[string]any) (*Indexer, error) {
	var path string
	if pathI, ok := m["path"]; ok {
		if path, ok = pathI.(string); !ok {
			return nil, errors.New("missing 'path' on asset indexer")
		}
	}
	return New(path), nil
}

// Indexer holds rpm packages
type Indexer struct {
	Type string `json:"type"`
	Path string `json:"path"`
}

// Init initializes the yum repository
func (o Indexer) Init(_ db.DB) error {
	if o.Path == "" {
		return errors.New("must set Path before yum.Init()")
	}
	if err := os.MkdirAll(o.Path, 0o700); err != nil {
		return err
	}
	return o.reindex()
}

func (o Indexer) reindex() error {
	slog.Info("reindexing yum repo", "path", o.Path)
	c, err := createrepo.NewRepo(o.Path, &createrepo.Config{
		WriteConfig: true,
	})
	if err != nil {
		return err
	}
	_, err = c.Create()
	return err
}

// Add adds a file from the given path to a repo
//
// Adding a file will create a subdirectory in the repo in the format of Packages/p/package-1.2.3.rpm
func (o Indexer) Add(source string) error {
	fn := path.Base(source)
	targetDir := path.Join(o.Path, "Packages", fn[0:1])
	if err := os.MkdirAll(targetDir, 0o700); err != nil {
		return err
	}
	dest := path.Join(targetDir, fn)
	slog.Info("renaming", "source", source, "dest", dest)
	if err := os.Rename(source, dest); err != nil {
		return err
	}
	return o.reindex()
}

// String returns the repo as a string
func (o Indexer) String() string {
	return "yum"
}
