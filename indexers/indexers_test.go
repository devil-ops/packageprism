package indexers

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestNames(t *testing.T) {
	require.Equal(t,
		Names(),
		[]string{"apt", "asset", "yum"},
	)
}

func TestUnknownIndexer(t *testing.T) {
	got, err := Create("never-exists", "/foo/bar")
	require.EqualError(t,
		err,
		"unknown indexer: never-exists",
	)
	require.Nil(t, got)
}

func TestMustCreate(t *testing.T) {
	require.Equal(t,
		MustCreate("apt", "/foo/bar").String(),
		"apt",
	)
}
