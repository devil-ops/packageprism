package apt

import (
	"encoding/json"
	"log/slog"
	"os"
	"path"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.oit.duke.edu/devil-ops/packageprism/db"
	"gitlab.oit.duke.edu/devil-ops/packageprism/db/builtin"
)

func init() {
	slog.SetDefault(slog.New(slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{
		Level: slog.LevelDebug,
	})))
}

func tmpDB(t *testing.T) db.DB {
	return builtin.MustNew(t.TempDir())
}

func tmpApt(t *testing.T, id string) (*Indexer, string) {
	d := t.TempDir() + "/" + id
	a := New(d)
	a.Init(tmpDB(t))
	td := time.Date(2009, 11, 17, 20, 34, 58, 651387237, time.UTC)
	a.Date = &td
	require.NoError(t, a.Init(builtin.MustNew(t.TempDir())))
	return a, d
}

func TestNew(t *testing.T) {
	require.NotNil(t, New("/foo/bar"))
}

func TestInit(t *testing.T) {
	_, d := tmpApt(t, "test-init")
	require.FileExists(t, path.Join(d, "test-init.asc"))
}

func TestMarshalReleaseLegacy(t *testing.T) {
	a, _ := tmpApt(t, "test-init")
	got, err := a.marshalReleaseLegacy("test-marshal-legacy", "amd64")
	require.NoError(t, err)
	require.Equal(t,
		`Component: test-marshal-legacy
Architecture: amd64
`,
		string(got),
	)
}

func TestMarshalRelease(t *testing.T) {
	a, _ := tmpApt(t, "test-marshal")
	testpkg64 := "../../testdata/packages/helloworld_1.0.0_amd64.deb"
	testpkgBase64 := path.Base(testpkg64)
	tpath64 := path.Join(t.TempDir(), testpkgBase64)
	require.NoError(t, copyFile(testpkg64, tpath64))
	require.NoError(t, a.Add(tpath64))
	got, err := a.marshalRelease()
	require.NoError(t, err)
	require.Equal(t,
		`Suite: stable
Codename: packageprism
Date: Tue, 17 Nov 2009 20:34:58 UTC
Architectures: amd64
Components: main
MD5Sum:
 bfe8b370e615fdd33d40704dfcdcc49e 461 main/binary-amd64/Packages
 14ca22a3b297f6547cd60e1446ea58ce 354 main/binary-amd64/Packages.gz
 7d20e7a3b53d9b553d18be7e5921c469 424 main/binary-amd64/Packages.xz
 063287201b2354699acfe229a752201d  36 main/binary-amd64/Release
SHA1:
 a7491f3e974727f3a3646ff644b8d7df4530ccd7 461 main/binary-amd64/Packages
 4c614eb5a94a4520d6c3b083ea63200981c0dadb 354 main/binary-amd64/Packages.gz
 1709cceb20a3d64945de2ccf00acc274b015f4eb 424 main/binary-amd64/Packages.xz
 f9082edea14092794f38390390e06abf8a922f7d  36 main/binary-amd64/Release
SHA256:
 489261f7d32915b5171050e162bcc2cd728c6af3362cc950230c339f40bd5865 461 main/binary-amd64/Packages
 2767ea7cd719639846e5671ea3d16951bca9e2f807e53ef85dba31726262e2c0 354 main/binary-amd64/Packages.gz
 fb02397b10e540e5fd01ebfedb86cc088db00491ec556b2c7f2959297102f4ff 424 main/binary-amd64/Packages.xz
 f4aafa353a338baedf9e98523c13d7ffeda979ec3cf745f9fad977dd770595c8  36 main/binary-amd64/Release
`,
		string(got),
	)
}

func TestAptAddFlat(t *testing.T) {
	testpkg := "../../testdata/packages/helloworld_1.0.0_amd64.deb"

	// Storage Directory to use here

	rt, d := tmpApt(t, "test-apt-addflat")
	rt.Flat = true

	tp := t.TempDir()
	testpkgBase := path.Base(testpkg)
	tpath := path.Join(tp, testpkgBase)
	require.NoError(t, copyFile(testpkg, tpath))
	packagePath := path.Join(d, "pool/h", "helloworld", testpkgBase)

	require.NoError(t, rt.Add(tpath))
	assert.FileExists(t, packagePath)
}

func TestDebInfo(t *testing.T) {
	rt, _ := tmpApt(t, "test-deb-info")

	got, err := rt.newDebPkg("../../testdata/packages/helloworld_1.0.0_amd64.deb")
	require.NoError(t, err)
	require.NotNil(t, got)
	assert.Equal(t, got.ID(), "helloworld_1.0.0_amd64.deb")

	assert.Contains(
		t,
		string(got.Marshal()),
		"Section: default\n",
	)
}

func TestPkgDBEntries(t *testing.T) {
	rt, _ := tmpApt(t, "test")

	require.NoError(t, rt.Init(builtin.MustNew(t.TempDir())))

	require.NoError(t, rt.addPkg(
		fromPTR(rt.mustNewDebPkg("../../testdata/packages/helloworld_1.0.0_amd64.deb")),
		fromPTR(rt.mustNewDebPkg("../../testdata/packages/goodbyeworld_1.0.0_amd64.deb")),
	))
	gotP, err := rt.getPkg("helloworld_1.0.0_amd64.deb")
	require.NoError(t, err)
	require.NotNil(t, gotP)
	require.Equal(t, "pool/h/helloworld/helloworld_1.0.0_amd64.deb", gotP.Filename)

	pkgs, err := rt.getAllPkgs()
	require.NoError(t, err)
	require.Len(t, pkgs, 2)
}

func TestMarshalReleases(t *testing.T) {
	rt, _ := tmpApt(t, "test-release")

	require.NoError(t, rt.addPkg(
		fromPTR(rt.mustNewDebPkg("../../testdata/packages/helloworld_1.0.0_i386.deb")),
		fromPTR(rt.mustNewDebPkg("../../testdata/packages/goodbyeworld_1.0.0_amd64.deb")),
	))
	require.NoError(t, rt.reindex())

	got, err := rt.marshalRelease()
	require.NoError(t, err)
	gotS := string(got)
	require.Contains(t, gotS, "Architectures: amd64 i386\n")
}

func TestGetAptRepoMetaFiles(t *testing.T) {
	rt, _ := tmpApt(t, "test-release")

	require.NoError(t, rt.addPkg(
		fromPTR(rt.mustNewDebPkg("../../testdata/packages/helloworld_1.0.0_i386.deb")),
		fromPTR(rt.mustNewDebPkg("../../testdata/packages/goodbyeworld_1.0.0_amd64.deb")),
	))
	require.NoError(t, rt.reindex())

	got, err := rt.getRepoMetaFiles()
	require.NoError(t, err)
	require.Equal(t,
		[]string{
			"dists/test-release/main/binary-amd64/Packages",
			"dists/test-release/main/binary-amd64/Packages.gz",
			"dists/test-release/main/binary-amd64/Packages.xz",
			"dists/test-release/main/binary-amd64/Release",
			"dists/test-release/main/binary-i386/Packages",
			"dists/test-release/main/binary-i386/Packages.gz",
			"dists/test-release/main/binary-i386/Packages.xz",
			"dists/test-release/main/binary-i386/Release",
		},
		got,
	)
}

func TestRepoMetaHashInfo(t *testing.T) {
	repoID := "test-meta-hashing"

	a, _ := tmpApt(t, repoID)

	require.NoError(t, a.addPkg(
		fromPTR(a.mustNewDebPkg("../../testdata/packages/helloworld_1.0.0_i386.deb")),
		fromPTR(a.mustNewDebPkg("../../testdata/packages/goodbyeworld_1.0.0_amd64.deb")),
	))
	require.NoError(t, a.reindex())

	got, err := a.generateRepoMetaHashInfo(path.Join("dists/test-meta-hashing/main/binary-amd64/Packages"))
	require.NoError(t, err)
	require.Equal(t,
		&repoMetaHashInfo{
			filename: "main/binary-amd64/Packages",
			size:     469,
			hashes: map[string]string{
				"MD5Sum": "c852afbd731c18dd5ff12980b67c6418",
				"SHA1":   "97d5d5067a98f63c71a912f7d151eec0853a754f",
				"SHA256": "5d2a719d31e1676f912b4f7d21142558d1838a2737f3e27e0c8ab374743c941a",
			},
		},
		got,
	)

	gotI, err := a.getRepoMetaHashInfo()
	require.NoError(t, err)
	require.Greater(t, len(gotI), 5)
}

func TestHashMarshalling(t *testing.T) {
	item := repoMetaHashInfoList{
		{
			filename: "main/binary-amd64/Packages",
			size:     467005,
			hashes: map[string]string{
				"MD5Sum": "a3f5045659f8194dcbb9c9dbc0750fac",
				"SHA1":   "6cf6d589e47de4acec2a6cc671f7f41e07b709a9",
				"SHA256": "3d369aeef8e961779e66bfe16c206075775cf1c23d234c74e90cc49972ab0b25",
			},
		},
		{
			filename: "main/binary-amd64/Packages.gz",
			size:     400,
			hashes: map[string]string{
				"MD5Sum": "9563c8e195919ea44e5ac363a4f75c22",
				"SHA1":   "ce40d378c33d7936927b5b2e0575117b0542798c",
				"SHA256": "8d1c3c38286607ac1d02976714052af4df05fdb70ef9be33d68de4bbdb5bd6e6",
			},
		},
	}

	got, err := item.marshal()
	require.NoError(t, err)
	require.Equal(t,
		`MD5Sum:
 a3f5045659f8194dcbb9c9dbc0750fac 467005 main/binary-amd64/Packages
 9563c8e195919ea44e5ac363a4f75c22    400 main/binary-amd64/Packages.gz
SHA1:
 6cf6d589e47de4acec2a6cc671f7f41e07b709a9 467005 main/binary-amd64/Packages
 ce40d378c33d7936927b5b2e0575117b0542798c    400 main/binary-amd64/Packages.gz
SHA256:
 3d369aeef8e961779e66bfe16c206075775cf1c23d234c74e90cc49972ab0b25 467005 main/binary-amd64/Packages
 8d1c3c38286607ac1d02976714052af4df05fdb70ef9be33d68de4bbdb5bd6e6    400 main/binary-amd64/Packages.gz
`,
		string(got),
	)
}

func TestReleaseFiles(t *testing.T) {
	a, _ := tmpApt(t, "test-release-files")

	require.NoError(t, a.Add(mustCopyF(t, "../../testdata/packages/helloworld_1.0.0_i386.deb")))
	require.NoError(t, a.Add(mustCopyF(t, "../../testdata/packages/helloworld_1.0.0_amd64.deb")))
	require.NoError(t, a.reindex())
	assert.FileExists(t, path.Join(a.Path, "test-release-files.asc"))
	assert.FileExists(t, path.Join(a.Path, "dists/test-release-files/Release"))
	assert.FileExists(t, path.Join(a.Path, "dists/test-release-files/Release.gpg"))
	assert.FileExists(t, path.Join(a.Path, "dists/test-release-files/InRelease"))

	assert.FileExists(t, path.Join(a.Path, "dists/test-release-files/main/binary-amd64/Release"))
	assert.FileExists(t, path.Join(a.Path, "dists/test-release-files/main/binary-amd64/Packages"))
	assert.FileExists(t, path.Join(a.Path, "dists/test-release-files/main/binary-amd64/Packages.gz"))
	assert.FileExists(t, path.Join(a.Path, "dists/test-release-files/main/binary-amd64/Packages.xz"))

	assert.FileExists(t, path.Join(a.Path, "dists/test-release-files/main/Contents-amd64.gz"))
	assert.FileExists(t, path.Join(a.Path, "dists/test-release-files/main/Contents-i386.gz"))
}

func TestDebFiles(t *testing.T) {
	a, _ := tmpApt(t, "test-deb-files")
	require.Equal(t,
		[]string{"usr/bin/helloworld"},
		a.mustNewDebPkg("../../testdata/packages/helloworld_1.0.0_i386.deb").Files,
	)
}

func TestContentsMarshal(t *testing.T) {
	require.Equal(t,
		"FILE   LOCATION\nusr/bin/hellooworld admin/helloworld\n",
		string(contentsPieceList{
			{
				file:     "usr/bin/hellooworld",
				packages: []string{"admin/helloworld"},
			},
		}.marshal()),
	)
}

func TestStripDist(t *testing.T) {
	require.Equal(t,
		"main/binary-amd64/Packages",
		stripDist("dists/test-strip/main/binary-amd64/Packages"),
	)
}

func mustCopyF(t *testing.T, in string) string {
	got, err := copyF(t, in)
	if err != nil {
		panic(err)
	}
	return got
}

func copyF(t *testing.T, in string) (string, error) {
	out := path.Join(t.TempDir(), path.Base(in))
	if err := copyFile(in, out); err != nil {
		return "", err
	}
	return out, nil
}

func copyFile(in, out string) error {
	i, e := os.Open(path.Clean(in))
	if e != nil {
		return e
	}
	defer dclose(i)
	o, e := os.Create(path.Clean(out))
	if e != nil {
		return e
	}
	defer dclose(o)
	_, err := o.ReadFrom(i)
	return err
}

func TestMarshal(t *testing.T) {
	// Test apt
	apt, _ := tmpApt(t, "test-marshal")
	got, err := json.Marshal(apt)
	require.NoError(t, err)
	assert.Contains(
		t,
		string(got),
		`"type":"apt","id":"test-marshal"`,
	)
}

func TestNewWithMap(t *testing.T) {
	got, err := NewWithMap(map[string]any{
		"path":       "/foo/bar/baz",
		"components": []string{"main"},
		"id":         "some-id",
		"origin":     "Some Origin",
		"label":      "Some Label",
		"suite":      "stable",
		"version":    "some-version",
		"codename":   "some-codename",
		"flat":       true,
		// "date":        "2009-11-17T20:34:58.0-00:00",
		"date":        "2009-11-17T20:34:58Z",
		"description": "some-description",
		"email":       "email@localhost.local",
	})
	require.NoError(t, err)
	require.Equal(t,
		&Indexer{
			Type:        "apt",
			ID:          "some-id",
			Flat:        true,
			Path:        "/foo/bar/baz",
			Components:  []string{"main"},
			Origin:      "Some Origin",
			Label:       "Some Label",
			Suite:       "stable",
			Version:     "some-version",
			Codename:    "some-codename",
			Description: "some-description",
			Email:       "email@localhost.local",
			Date:        toPTR(time.Date(2009, 11, 17, 20, 34, 58, 0, time.UTC)),
		},
		got,
	)
}
