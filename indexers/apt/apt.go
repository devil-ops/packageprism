/*
Package apt handles apt related operations
*/
package apt

import (
	"bytes"
	"cmp"
	"compress/gzip"
	"crypto/md5"  //nolint:gosec
	"crypto/sha1" //nolint:gosec
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"log/slog"
	"os"
	"path"
	"path/filepath"
	"slices"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/ProtonMail/gopenpgp/v2/crypto"
	"github.com/ProtonMail/gopenpgp/v2/helper"
	"github.com/ulikunitz/xz"
	"gitlab.oit.duke.edu/devil-ops/packageprism/db"

	"github.com/isbm/go-deb"
)

const (
	// These are used for the title of the hashes in the Release type files
	md5name    = "MD5Sum"
	sha1name   = "SHA1"
	sha256name = "SHA256"

	// Part of the apt standard in a few spots
	binPrefix = "binary-"
)

// Indexer holds deb pakages
type Indexer struct {
	db            db.DB
	Type          string     `json:"type"`
	ID            string     `json:"id,omitempty"`
	Flat          bool       `json:"flat,omitempty"`
	Components    []string   `json:"components"`
	Architectures []string   `json:"architectures"`
	Path          string     `json:"path"`
	Origin        string     `json:"origin,omitempty"`
	Label         string     `json:"label,omitempty"`
	Suite         string     `json:"suite,omitempty"`
	Version       string     `json:"version,omitempty"`
	Codename      string     `json:"codename,omitempty"`
	Date          *time.Time `json:"date,omitempty"`
	Description   string     `json:"description,omitempty"`
	Email         string     `json:"email,string"`
	PrivateKey    string     `json:"private_key,omitempty"`
	PublicKey     string     `json:"public_key,omitempty"`
}

// New returns a new Apt Indexers
func New(p string) *Indexer {
	return &Indexer{
		Type: "apt",
		Path: p,
	}
}

// Init initializes the apt repository. This should just get the base stuff needed for an empty repo
func (o *Indexer) Init(db db.DB) error {
	slog.Debug("initializing AptIndexer", "path", o.Path)
	// Ensure that the base directory exists
	if err := os.MkdirAll(path.Join(o.Path, "pool"), 0o700); err != nil {
		return err
	}

	// Setup the db interface
	o.db = db

	// Is this the best way to set the ID? Probably not...
	o.ID = path.Base(o.Path)

	if o.Date == nil {
		n := time.Now()
		o.Date = &n
	}

	if len(o.Architectures) == 0 {
		o.Architectures = []string{"amd64"}
	}

	if len(o.Components) == 0 {
		o.Components = []string{"main"}
	}
	if err := o.generateKeyPair(); err != nil {
		return err
	}

	slog.Info("Creating signing pubkey", "path", path.Join(o.Path, o.ID+".asc"))
	if err := os.WriteFile(path.Join(o.Path, o.ID+".asc"), []byte(o.PublicKey), 0o600); err != nil {
		return err
	}
	// return o.repo.commit()
	return nil
}

func (o *Indexer) marshalReleaseLegacy(component, arch string) ([]byte, error) {
	ret := bytes.NewBufferString("")
	for _, item := range []struct{ k, v string }{
		{k: "Archive", v: o.Suite}, // Archive is the old name for 'Suite'
		{k: "Origin", v: o.Origin},
		{k: "Label", v: o.Label},
	} {
		optionalWriteStr(ret, item.k, item.v)
	}
	if err := writeStr(ret, "Component", component); err != nil {
		return nil, err
	}
	if err := writeStr(ret, "Architecture", arch); err != nil {
		return nil, err
	}

	return ret.Bytes(), nil
}

func (o *Indexer) marshalRelease() ([]byte, error) {
	ret := bytes.NewBufferString("")

	// Optional Fields
	for _, item := range []struct{ k, v string }{
		{k: "Description", v: o.Description},
		{k: "Origin", v: o.Origin},
		{k: "Label", v: o.Label},
		{k: "Version", v: o.Version},
		{k: "Suite", v: cmp.Or(o.Suite, "stable")},
		{k: "Codename", v: cmp.Or(o.Codename, "packageprism")},
	} {
		optionalWriteStr(ret, item.k, item.v)
	}

	if o.Date != nil {
		optionalWriteStr(ret, "Date", o.Date.Format(time.RFC1123))
	}

	// These 2 are required
	if werr := writeStrs(ret, "Architectures", o.Architectures); werr != nil {
		return nil, werr
	}

	if cerr := writeStrs(ret, "Components", o.Components); cerr != nil {
		return nil, cerr
	}
	hashes, err := o.getRepoMetaHashInfo()
	if err != nil {
		return nil, err
	}
	// TODO: Generate the acquire--by-hash files here here

	hashesB, err := hashes.marshal()
	if err != nil {
		return nil, err
	}
	if _, err := ret.Write(hashesB); err != nil {
		return nil, err
	}

	return ret.Bytes(), nil
}

// Add adds a file from the given path to a repo
func (o Indexer) Add(source string) error {
	// Set options manually or use deb.DefaultPackageOptions instance
	options := &deb.PackageOptions{
		Hash:                 deb.HASH_SHA1,
		RecalculateChecksums: true,
		MetaOnly:             false,
	}

	p, err := deb.OpenPackageFile(source, options)
	if err != nil {
		return err
	}

	targetDir := path.Join(o.Path, "pool", p.ControlFile().Package()[0:1], p.ControlFile().Package())
	if err := os.MkdirAll(targetDir, 0o700); err != nil {
		return err
	}
	targetFile := path.Join(targetDir, path.Base(source))
	slog.Info("moving deb file", "source", source, "dest", targetFile)
	if err := os.Rename(source, targetFile); err != nil {
		return err
	}

	// return nil
	return o.reindex()
}

// String returns the repo as a string
func (Indexer) String() string {
	return "apt"
}

func (o *Indexer) scanPool() error {
	if err := filepath.Walk(path.Join(o.Path, "pool"), func(path string, _ fs.FileInfo, _ error) error {
		if !strings.HasSuffix(path, ".deb") {
			return nil
		}

		info, err := o.newDebPkg(path)
		if err != nil {
			return err
		}
		if err := o.addPkg(*info); err != nil {
			return err
		}
		if !slices.Contains(o.Architectures, info.Architecture) {
			o.Architectures = append(o.Architectures, info.Architecture)
		}

		return nil
	}); err != nil {
		return err
	}

	return nil
}

func (o *Indexer) reindex() error {
	slog.Info("reindexing apt repo", "path", o.Path)

	if err := o.scanPool(); err != nil {
		return err
	}

	return o.writeMetadata()
}

type repoMetaHashInfo struct {
	filename string
	size     int64
	hashes   map[string]string
}

type repoMetaHashInfoList []repoMetaHashInfo

func (r repoMetaHashInfoList) marshal() ([]byte, error) {
	inv := map[string][][3]string{}
	if len(r) == 0 {
		return nil, fmt.Errorf("cannot marshal empty repoMetaHashInfoList")
	}
	for _, item := range r {
		if len(item.hashes) == 0 {
			return nil, fmt.Errorf("missing hashes in list: %v", item.filename)
		}
		for hashName, hash := range item.hashes {
			inv[hashName] = append(inv[hashName], [3]string{
				hash,
				strconv.FormatInt(item.size, 10),
				item.filename,
			})
		}
	}

	ret := bytes.NewBufferString("")
	for _, hashName := range []string{md5name, sha1name, sha256name} {
		hashes, ok := inv[hashName]
		if !ok {
			return nil, fmt.Errorf("missing hash type in list: %v", hashName)
		}
		maxSize := int(0)
		for _, hash := range hashes {
			size := len(hash[1])
			if size > maxSize {
				maxSize = size
			}
		}
		fmt.Fprintf(ret, "%v:\n", hashName)
		for _, hash := range hashes {
			size := len(hash[1])
			padding := maxSize - size

			fmt.Fprintf(ret, " %v %v%v %v\n", hash[0], strings.Repeat(" ", padding), hash[1], hash[2])
		}
	}
	return ret.Bytes(), nil
}

func stripDist(s string) string {
	return strings.SplitAfterN(s, "/", 3)[2]
}

func (o Indexer) getRepoMetaHashInfo() (repoMetaHashInfoList, error) {
	metaFs, err := o.getRepoMetaFiles()
	if err != nil {
		return nil, err
	}
	ret := repoMetaHashInfoList{}
	for _, fn := range metaFs {
		repoMetaHashInfo, err := o.generateRepoMetaHashInfo(fn)
		if err != nil {
			slog.Error("could not generate metadata", "file", fn, "error", err)
			continue
		}
		ret = append(ret, *repoMetaHashInfo)
	}
	return ret, nil
}

func (o Indexer) generateRepoMetaHashInfo(fn string) (*repoMetaHashInfo, error) {
	fullFn := path.Join(o.Path, fn)
	stat, err := os.Stat(fullFn)
	if err != nil {
		return nil, err
	}

	md5hash := md5.New()   //nolint:gosec
	sha1hash := sha1.New() //nolint:gosec
	sha256hash := sha256.New()

	multiWriter := io.MultiWriter(md5hash, sha1hash, sha256hash)
	f, err := os.Open(path.Clean(fullFn))
	if err != nil {
		return nil, err
	}
	defer dclose(f)

	if _, err := io.Copy(multiWriter, f); err != nil {
		return nil, err
	}

	return &repoMetaHashInfo{
		filename: stripDist(fn),
		size:     stat.Size(),
		hashes: map[string]string{
			md5name:    hex.EncodeToString(md5hash.Sum(nil)),
			sha1name:   hex.EncodeToString(sha1hash.Sum(nil)),
			sha256name: hex.EncodeToString(sha256hash.Sum(nil)),
		},
	}, nil
}

func (o *Indexer) getRepoMetaFiles() ([]string, error) {
	// No special files for flat repos
	if o.Flat {
		return []string{}, nil
	}
	files := []string{}
	for _, arch := range o.Architectures {
		for _, component := range o.Components {
			files = append(files,
				path.Join(o.componentPath(component), binPrefix+arch, "Packages"),
				path.Join(o.componentPath(component), binPrefix+arch, "Packages.gz"),
				path.Join(o.componentPath(component), binPrefix+arch, "Packages.xz"),
				path.Join(o.componentPath(component), binPrefix+arch, "Release"),
			)
		}
	}
	if len(files) == 0 {
		return nil, errors.New("no MetaFiles found")
	}
	sort.Strings(files)
	return files, nil
}

func (o *Indexer) distPath() string {
	return path.Join(
		"dists",
		o.ID,
	)
}

func (o *Indexer) distFSPath() string {
	return path.Join(
		o.Path,
		o.distPath(),
	)
}

func (o *Indexer) componentPath(c string) string {
	return path.Join(
		o.distPath(),
		c,
	)
}

func (o *Indexer) componentFSPath(c string) string {
	return path.Join(
		o.distFSPath(),
		c,
	)
}

func (o *Indexer) writePackageIndices() error {
	pkgs, err := o.getAllPkgs()
	if err != nil {
		return err
	}

	// Flat repositories just go splat in the top level dir
	if o.Flat {
		return o.writeCompressedFiles(cfileConfig{
			name:         "Packages",
			data:         pkgs.Marshal(),
			directory:    o.Path,
			uncompressed: true,
			compressions: []string{"gz", "xz"},
		})
	}

	for _, component := range o.Components {
		for _, arch := range pkgs.architectures() {
			ppath := path.Join(o.componentFSPath(component), binPrefix+arch)
			if err := os.MkdirAll(ppath, 0o700); err != nil {
				return nil
			}
			// if err := r.writeCompressedFiles(pkgs.MarshalWithArch(arch), ppath, "Packages"); err != nil {
			if err := o.writeCompressedFiles(cfileConfig{
				name:         "Packages",
				data:         pkgs.MarshalWithArch(arch),
				directory:    ppath,
				uncompressed: true,
				compressions: []string{"gz", "xz"},
			}); err != nil {
				return err
			}
		}
	}
	return nil
}

func (o *Indexer) writeReleaseFiles() error {
	slog.Info("about to create Release files", "path", o.distFSPath())
	if err := o.writeReleasesFS(); err != nil {
		return err
	}

	// Now do Legacy Release files here
	pkgs, err := o.getAllPkgs()
	if err != nil {
		return err
	}
	for _, component := range o.Components {
		for _, arch := range pkgs.architectures() {
			lb, err := o.marshalReleaseLegacy(component, arch)
			if err != nil {
				return err
			}
			if err := o.writeFile(path.Join(o.componentPath(component), binPrefix+arch, "Release"), lb); err != nil {
				return err
			}
		}
	}
	return nil
}

// writeFile writes out a file using the default file mode, and auto prefixes the
// repository base path
func (o Indexer) writeFile(name string, data []byte) error {
	targetFile := path.Join(o.Path, name)
	targetDir := path.Dir(targetFile)
	if err := os.MkdirAll(targetDir, 0o700); err != nil {
		return err
	}
	slog.Info("Writing file", "file", targetFile, "path", targetDir)
	return os.WriteFile(path.Join(o.Path, name), data, 0o600)
}

func (o *Indexer) writeMetadata() error {
	if err := o.writePackageIndices(); err != nil {
		return err
	}

	if err := o.writeContentsIndices(); err != nil {
		return err
	}

	if !o.Flat {
		if err := o.writeReleaseFiles(); err != nil {
			return err
		}
	}

	return nil
}

func (o *Indexer) writeContentsIndices() error {
	pkgs, err := o.getAllPkgs()
	if err != nil {
		return err
	}
	for _, arch := range o.Architectures {
		for _, component := range o.Components {
			slog.Info("generating contents for", "component", component, "arch", arch)
			pieces := pkgs.contentPieces(arch)
			tdir := path.Join(o.componentFSPath(component))
			if err := o.writeCompressedFiles(cfileConfig{
				name:         "Contents-" + arch,
				compressions: []string{"gz"},
				data:         pieces.marshal(),
				directory:    tdir,
			}); err != nil {
				return err
			}
			slog.Info("Wrote compressed Contents", "directory", tdir)
		}
	}
	return nil
}

func (o *Indexer) writeReleasesFS() error {
	rB, merr := o.marshalRelease()
	if merr != nil {
		return merr
	}
	if err := o.writeFile(path.Join(o.distPath(), "Release"), rB); err != nil {
		return err
	}

	signingKey, err := crypto.NewKeyFromArmored(o.PrivateKey)
	if err != nil {
		return err
	}
	keyRing, err := crypto.NewKeyRing(signingKey)
	if err != nil {
		return err
	}

	pgpSignature, err := keyRing.SignDetached(crypto.NewPlainMessageFromString(string(rB)))
	if err != nil {
		return err
	}
	armoredSig, err := pgpSignature.GetArmored()
	if err != nil {
		return err
	}
	if werr := o.writeFile(path.Join(o.distPath(), "Release.gpg"), []byte(armoredSig+"\n")); werr != nil {
		return werr
	}

	signed, sierr := helper.SignCleartextMessage(keyRing, string(rB))
	if sierr != nil {
		return sierr
	}

	if serr := o.writeFile(path.Join(o.distPath(), "InRelease"), []byte(signed)); serr != nil {
		return serr
	}

	return nil
}

// Compressed File Writing Config
type cfileConfig struct {
	data         []byte
	directory    string
	name         string
	compressions []string
	uncompressed bool
}

func (o *Indexer) writeCompressedFiles(c cfileConfig) error {
	if err := os.MkdirAll(c.directory, 0o700); err != nil {
		return err
	}
	writers := []io.Writer{}
	if c.uncompressed {
		tfile := path.Join(c.directory, c.name)
		slog.Info("Writing uncompressed file", "path", tfile)
		pw, err := os.Create(path.Clean(tfile))
		if err != nil {
			return err
		}
		defer dclose(pw)
		writers = append(writers, pw)
	}

	if slices.Contains(c.compressions, "gz") {
		tfile := path.Join(c.directory, c.name+".gz")
		slog.Info("Writing gz compressed file", "path", tfile)
		cf, err := os.Create(path.Clean(tfile))
		if err != nil {
			return err
		}
		defer dclose(cf)
		cw := gzip.NewWriter(cf)
		defer dclose(cw)
		writers = append(writers, cw)
	}
	if slices.Contains(c.compressions, "xz") {
		tfile := path.Join(c.directory, c.name+".xz")
		slog.Info("Writing xz compressed file", "path", tfile)
		xf, err := os.Create(path.Clean(tfile))
		if err != nil {
			return err
		}
		defer dclose(xf)
		xw, err := xz.NewWriter(xf)
		if err != nil {
			return err
		}
		defer dclose(xw)
		writers = append(writers, xw)
	}

	mw := io.MultiWriter(writers...)
	_, err := mw.Write(c.data)

	return err
}

type contentsPiece struct {
	file     string
	packages []string
}

type contentsPieceList []contentsPiece

func (c contentsPieceList) marshal() []byte {
	ret := bytes.NewBufferString("")
	fmt.Fprint(ret, "FILE   LOCATION\n")
	for _, item := range c {
		fmt.Fprintf(ret, "%v %v\n", item.file, strings.Join(item.packages, ","))
	}
	return ret.Bytes()
}

func (o *Indexer) mustNewDebPkg(fn string) *debPkg {
	got, err := o.newDebPkg(fn)
	if err != nil {
		panic(err)
	}
	return got
}

func (o *Indexer) newDebPkg(fn string) (*debPkg, error) {
	stat, err := os.Stat(fn)
	if err != nil {
		return nil, err
	}
	p, err := deb.OpenPackageFile(fn, &deb.PackageOptions{
		Hash:                 deb.HASH_SHA1,
		RecalculateChecksums: true,
		MetaOnly:             false,
	})
	if err != nil {
		return nil, err
	}
	files := []string{}
	for _, f := range p.Files() {
		fname := f.String()
		if strings.HasSuffix(fname, "/") {
			continue
		}
		files = append(files, strings.TrimPrefix(fname, "./"))
	}
	base := path.Base(fn)
	return &debPkg{
		Package:       p.ControlFile().Package(),
		Version:       p.ControlFile().Version(),
		Architecture:  p.ControlFile().Architecture(),
		Maintainer:    p.ControlFile().Maintainer(),
		InstalledSize: p.ControlFile().InstalledSize(),
		Filename:      path.Join("pool", base[0:1], p.ControlFile().Package(), base),
		Size:          stat.Size(),
		MD5Sum:        p.GetPackageChecksum().MD5(),
		SHA1:          p.GetPackageChecksum().SHA1(),
		SHA256:        p.GetPackageChecksum().SHA256(),
		Section:       p.ControlFile().Section(),
		Priority:      p.ControlFile().Priority(),
		Description:   p.ControlFile().Description(),
		Depends:       strings.Join(p.ControlFile().Depends(), ", "),
		PreDepends:    strings.Join(p.ControlFile().Predepends(), ", "),
		Conflicts:     strings.Join(p.ControlFile().Conflicts(), ", "),
		Files:         files,
	}, nil
}

type debPkg struct {
	Filename      string   `json:"filename,omitempty"`
	Package       string   `json:"package,omitempty"`
	Version       string   `json:"version,omitempty"`
	Architecture  string   `json:"architecture,omitempty"`
	Maintainer    string   `json:"maintainer,omitempty"`
	InstalledSize int      `json:"installed_size,omitempty"`
	Size          int64    `json:"size,omitempty"`
	MD5Sum        string   `json:"md5sum,omitempty"`
	SHA1          string   `json:"sha1,omitempty"`
	SHA256        string   `json:"sha256,omitempty"`
	Section       string   `json:"section,omitempty"`
	Priority      string   `json:"priority,omitempty"`
	Description   string   `json:"description,omitempty"`
	Depends       string   `json:"depends,omitempty"`
	PreDepends    string   `json:"pre_depends,omitempty"`
	Conflicts     string   `json:"conflicts,omitempty"`
	Files         []string `json:"files,omitempty"`
}

func (d debPkg) ID() string {
	return path.Base(d.Filename)
}

func (d debPkg) Marshal() []byte {
	ret := bytes.NewBufferString("")
	ret.WriteString("Package: " + d.Package + "\n")
	ret.WriteString("Version: " + d.Version + "\n")
	ret.WriteString("Architecture: " + d.Architecture + "\n")
	ret.WriteString("Maintainer: " + d.Maintainer + "\n")
	ret.WriteString("Installed-Size: " + fmt.Sprint(d.InstalledSize) + "\n")
	ret.WriteString("Filename: " + d.Filename + "\n")
	ret.WriteString("Size: " + fmt.Sprint(d.Size) + "\n")
	ret.WriteString("MD5Sum: " + d.MD5Sum + "\n")
	ret.WriteString("SHA1: " + d.SHA1 + "\n")
	ret.WriteString("SHA256: " + d.SHA256 + "\n")
	ret.WriteString("Section: " + d.Section + "\n")
	ret.WriteString("Priority: " + d.Priority + "\n")
	ret.WriteString("Description: " + d.Description + "\n")
	ret.WriteString("\n")
	return ret.Bytes()
}

type debPkgList []debPkg

// Opportunity for improvement here?
// Instead of cycling through the db packages, why not just store the list
// and retrieve it when we need it?
func (l debPkgList) architectures() []string {
	ret := []string{}
	for _, pkg := range l {
		if !slices.Contains(ret, pkg.Architecture) {
			ret = append(ret, pkg.Architecture)
		}
	}
	return ret
}

func (l debPkgList) contentPieces(arch string) contentsPieceList {
	ret := contentsPieceList{}
	tmpMap := map[string][]string{}
	for _, pkg := range l {
		if pkg.Architecture != arch {
			continue
		}
		packageName := pkg.Section + "/" + pkg.Package

		for _, fn := range pkg.Files {
			if _, ok := tmpMap[fn]; !ok {
				tmpMap[fn] = []string{}
			}
			tmpMap[fn] = append(tmpMap[fn], packageName)
		}
	}
	for file, packages := range tmpMap {
		ret = append(ret, contentsPiece{
			file:     file,
			packages: packages,
		})
	}
	return ret
}

func (l debPkgList) Marshal() []byte {
	r := bytes.NewBufferString("")
	for _, item := range l {
		r.Write(item.Marshal())
	}
	return r.Bytes()
}

// MarshalWithArch marshals but only on architecture matching the given string
func (l debPkgList) MarshalWithArch(arch string) []byte {
	r := bytes.NewBufferString("")
	for _, item := range l {
		if item.Architecture == arch {
			r.Write(item.Marshal())
		}
	}
	return r.Bytes()
}

func (o *Indexer) getAllPkgs() (debPkgList, error) {
	ret := debPkgList{}
	if err := o.db.GetAll(db.PackageBucket, o.ID+"/", &ret); err != nil {
		return nil, err
	}
	return ret, nil
}

func (o *Indexer) getPkg(name string) (*debPkg, error) {
	var ret debPkg
	if err := o.db.Get(db.PackageBucket, o.ID+"/"+name, &ret); err != nil {
		return nil, err
	}
	return &ret, nil
}

func (o *Indexer) addPkg(pkgs ...debPkg) error {
	for _, pkg := range pkgs {
		if !slices.Contains(o.Architectures, pkg.Architecture) {
			o.Architectures = append(o.Architectures, pkg.Architecture)
		}
		if err := o.db.Set(db.PackageBucket, path.Join(o.ID, pkg.ID()), pkg); err != nil {
			return err
		}
	}
	return nil
}

func (o *Indexer) generateKeyPair() error {
	// Do we already have the keypair?
	if o.PrivateKey != "" {
		return nil
	}
	key, err := crypto.GenerateKey("packageprims-apt-key", cmp.Or(o.Email, fmt.Sprintf("root@%v.local", o.ID)), "rsa", 2048)
	if err != nil {
		return err
	}

	var pkerr error
	if o.PrivateKey, pkerr = key.Armor(); pkerr != nil {
		return pkerr
	}

	var pukerr error
	if o.PublicKey, pukerr = key.GetArmoredPublicKey(); pukerr != nil {
		return pukerr
	}

	return nil
}

func writeStrs(buf *bytes.Buffer, n string, s []string) error {
	if len(s) == 0 {
		return fmt.Errorf("got empty string on required field: %v", n)
	}
	_, err := buf.WriteString(n + ": " + strings.Join(s, " ") + "\n")
	return err
}

func writeStr(buf *bytes.Buffer, n string, s string) error {
	if s == "" {
		return fmt.Errorf("got empty string on required field: %v", n)
	}
	_, err := buf.WriteString(n + ": " + s + "\n")
	return err
}

func optionalWriteStr(buf *bytes.Buffer, n, s string) {
	if s != "" {
		buf.WriteString(n + ": " + s + "\n")
	}
}

func dclose(c io.Closer) {
	if err := c.Close(); err != nil {
		slog.Error("error closing item in apt indexer")
	}
}

func fromPTR[V any](v *V) V {
	return *v
}

func toPTR[V any](v V) *V {
	return &v
}

func getDateProperty(p map[string]any, k string) (*time.Time, error) {
	got, ok := p[k]
	if !ok {
		return toPTR(time.Time{}), nil
	}
	var d time.Time
	if d, ok = got.(time.Time); ok {
		return &d, nil
	}
	res, err := time.Parse(time.RFC3339Nano, got.(string))
	if err != nil {
		return nil, err
	}
	return &res, nil
	// return nil, fmt.Errorf("invalid date format: %v", d)
}

func getProperty[T string | bool | []string | time.Time](p map[string]any, k string) T {
	switch any(new(T)).(type) {
	case *string:
		got, ok := p[k]
		if !ok {
			return *new(T)
		} else {
			return got.(T)
		}
	case *bool:
		got, ok := p[k]
		if !ok {
			return *new(T)
		} else {
			return got.(T)
		}
	case *[]string:
		got, ok := p[k]
		if !ok {
			return *new(T)
		} else {
			if gotS, ok := got.([]string); ok {
				return any(gotS).(T)
			}
			gotA := got.([]any)
			ret := make([]string, len(gotA))
			for idx, item := range gotA {
				ret[idx] = fmt.Sprint(item)
			}
			return any(ret).(T)
		}
	default:
		panic("unknown type given to getProperty")
	}
}

// NewWithMap returns a new Apt indexer from a map[string]any
func NewWithMap(m map[string]any) (*Indexer, error) {
	o := New(getProperty[string](m, "path"))
	o.ID = getProperty[string](m, "id")
	o.Origin = getProperty[string](m, "origin")
	o.Label = getProperty[string](m, "label")
	o.Suite = getProperty[string](m, "suite")
	o.Version = getProperty[string](m, "version")
	o.Codename = getProperty[string](m, "codename")
	o.Description = getProperty[string](m, "description")
	o.Email = getProperty[string](m, "email")
	o.Flat = getProperty[bool](m, "flat")
	o.Components = getProperty[[]string](m, "components")
	o.PrivateKey = getProperty[string](m, "private_key")
	o.PublicKey = getProperty[string](m, "public_key")

	d, err := getDateProperty(m, "date")
	if err != nil {
		return nil, err
	}
	o.Date = d

	return o, nil
}
