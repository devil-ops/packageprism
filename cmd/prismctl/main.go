/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package main

import "gitlab.oit.duke.edu/devil-ops/packageprism/cmd/prismctl/cmd"

func main() {
	cmd.Execute()
}
