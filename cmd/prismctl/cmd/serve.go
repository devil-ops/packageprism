package cmd

import (
	"os"
	"os/signal"
	"path"
	"syscall"

	"github.com/spf13/cobra"
	prism "gitlab.oit.duke.edu/devil-ops/packageprism"
	"gopkg.in/yaml.v3"
)

func newServeCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:     "serve",
		Short:   "Run the Prism server",
		Aliases: []string{"server", "serv"},
		RunE: func(cmd *cobra.Command, _ []string) error {
			opts := []func(*prism.Server){}
			cf := mustGetCmd[string](*cmd, "config")
			if cf != "" {
				cb, err := os.ReadFile(path.Clean(cf))
				if err != nil {
					return err
				}
				var conf prism.Config
				if err := yaml.Unmarshal(cb, &conf); err != nil {
					return err
				}
				opts = append(opts, prism.WithConfig(conf))
			}

			srv := prism.MustNew(opts...)
			go func() {
				if err := srv.Run(); err != nil {
					logger.Error("fatal error", "error", err)
				}
			}()
			logger.Info("listener", "addr", srv.Addr())

			waitForCtrlC()
			return nil
		},
	}
	cmd.PersistentFlags().StringP("config", "c", "", "Configuration file for the server")
	return cmd
}

func waitForCtrlC() {
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	s := <-sig
	logger.Warn("signal received, stopping", "signal", s.String())
}
