/*
Package cmd is the cli thing
*/
package cmd

import (
	"log/slog"
	"os"
	"time"

	"github.com/charmbracelet/log"
	"github.com/spf13/cobra"
)

var (
	verbose bool
	logger  *slog.Logger
)

// rootCmd represents the base command when called without any subcommands
func newRootCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:           "prismctl",
		Short:         "Run a Package Prism web server",
		SilenceUsage:  true,
		SilenceErrors: true,
		PersistentPreRunE: func(_ *cobra.Command, _ []string) error {
			logger = slog.New(log.NewWithOptions(os.Stderr, newLoggerOpts()))
			slog.SetDefault(logger)
			return nil
		},
	}
	cmd.PersistentFlags().BoolVarP(&verbose, "verbose", "v", false, "Verbose output")
	cmd.AddCommand(
		newServeCmd(),
	)
	return cmd
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := newRootCmd().Execute(); err != nil {
		slog.Error("fatal error", "error", err)
		os.Exit(1)
	}
}

func newLoggerOpts() log.Options {
	logOpts := log.Options{
		ReportTimestamp: true,
		TimeFormat:      time.RFC3339,
		Prefix:          "packageprism  🌈💎📦 ",
		Level:           log.InfoLevel,
	}
	if verbose {
		logOpts.Level = log.DebugLevel
	}

	return logOpts
}
