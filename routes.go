package prism

import (
	"net/http"
	"slices"
)

func addRoutes(s *Server, mux *http.ServeMux) {
	// globalMids gets applied to EVERY handler
	globalMids := []middleware{
		handleLogsX,
	}
	// adminMids is just things that require an admin token
	adminMids := slices.Concat(globalMids, []middleware{s.validateTokenX})

	mux.HandleFunc("GET /public/{repo_id}/", multipleMiddleware(s.handleStaticRepo, globalMids...))
	mux.HandleFunc("GET /api/v1/repos", multipleMiddleware(s.handleListRepos, adminMids...))
	mux.HandleFunc("GET /api/v1/repos/{repo_id}", multipleMiddleware(s.handleGetRepo, adminMids...))
	mux.HandleFunc("POST /api/v1/repos/{repo_id}/packages/{package_name}", multipleMiddleware(s.handleAddPackage, adminMids...))
	mux.HandleFunc("/", multipleMiddleware(http.NotFound, globalMids...))
}
