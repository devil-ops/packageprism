/*
Package db describes how the database should be implemented
*/
package db

// DB describes the backend database for the server
type DB interface {
	// Init() error
	Set(string, string, any) error
	Get(string, string, any) error
	GetAll(string, string, any) error
}

const (
	// RepoBucket is the bucket for repository data
	RepoBucket = "repositories"
	// PackageBucket is the bucket for package data
	PackageBucket = "packages"
)
