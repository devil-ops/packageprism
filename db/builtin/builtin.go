package builtin

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log/slog"
	"os"
	"path"
	"strings"

	"gitlab.oit.duke.edu/devil-ops/packageprism/db"

	bolt "go.etcd.io/bbolt"
)

type localdb struct {
	dir string
	db  *bolt.DB
}

func MustNew(directory string) *localdb {
	got, err := New(directory)
	if err != nil {
		panic(err)
	}
	return got
}

// New returns a new database in the given directory
func New(directory string) (*localdb, error) {
	d := localdb{}
	if directory != "" {
		d.dir = directory
	} else {
		dir, err := os.MkdirTemp("", "builtindb")
		if err != nil {
			return nil, err
		}
		d.dir = dir
	}
	if err := d.init(); err != nil {
		return nil, err
	}
	return &d, nil
}

func (d *localdb) init() error {
	var dberr error
	d.db, dberr = bolt.Open(path.Join(d.dir, "prism.db"), 0o600, nil)
	if dberr != nil {
		return nil
	}
	// store some data
	return d.db.Update(func(tx *bolt.Tx) error {
		for _, item := range []string{string(db.RepoBucket), string(db.PackageBucket)} {
			bucket := tx.Bucket([]byte(item))
			if bucket == nil {
				if _, berr := tx.CreateBucket([]byte(item)); berr != nil {
					return berr
				}
			}
		}
		return nil
	})
}

func (d localdb) Set(bucket, key string, data any) error {
	return d.db.Update(func(tx *bolt.Tx) error {
		dataB, err := json.Marshal(data)
		if err != nil {
			return err
		}
		return tx.Bucket([]byte(bucket)).Put([]byte(key), dataB)
	})
}

func (d localdb) Get(bucket, key string, target any) error {
	slog.Debug("Looking up key", "bucket", bucket, "key", key)
	return d.db.View(func(tx *bolt.Tx) error {
		v := tx.Bucket([]byte(bucket)).Get([]byte(key))
		if string(v) == "" {
			return fmt.Errorf("key not found: %v:%v", bucket, key)
		}
		if err := json.Unmarshal(v, &target); err != nil {
			slog.Error("got error unmarshalling", "error", err)
			return err
		}
		return nil
	})
}

// GetAll returns all keys froma  given bucket
func (d localdb) GetAll(key, prefix string, target any) error {
	items := []string{}
	if err := d.db.View(func(tx *bolt.Tx) error {
		c := tx.Bucket([]byte(key)).Cursor()
		if prefix == "" {
			for k, v := c.First(); k != nil; k, v = c.Next() {
				items = append(items, string(v))
			}
		} else {
			prefixB := []byte(prefix)
			for k, v := c.Seek(prefixB); k != nil && bytes.HasPrefix(k, prefixB); k, v = c.Next() {
				items = append(items, string(v))
			}

		}
		return nil
	}); err != nil {
		return err
	}

	assembled := "[" + strings.Join(items, ",") + "]"
	return json.Unmarshal([]byte(assembled), &target)
}
