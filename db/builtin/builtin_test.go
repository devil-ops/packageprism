package builtin

import (
	"path"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestNew(t *testing.T) {
	d, err := New("")
	require.NoError(t, err)
	require.NotNil(t, d)
}

func TestInit(t *testing.T) {
	td := t.TempDir()
	d, err := New(td)
	require.NoError(t, err)
	// require.NoError(t, d.Init())
	require.FileExists(t, path.Join(d.db.Path()))
}

func TestSetGet(t *testing.T) {
	d := MustNew(t.TempDir())
	// require.NoError(t, d.Init())
	item := struct{ Name string }{Name: "Joe"}
	require.NoError(t, d.Set("repositories", "foo", item))
	got := struct{ Name string }{Name: "Joe"}
	require.NoError(t, d.Get("repositories", "foo", &got))

	require.EqualError(t, d.Get("repositories", "never-exists", &got), "key not found: repositories:never-exists")
}

func TestGetAll(t *testing.T) {
	d := MustNew(t.TempDir())
	// require.NoError(t, d.Init())

	require.NoError(t, d.Set("repositories", "mom", struct{ X string }{X: "Ellen"}))
	require.NoError(t, d.Set("repositories", "dad", struct{ X string }{X: "Joe"}))

	var expect []struct{ X string }
	require.NoError(t, d.GetAll("repositories", "", &expect))
	require.Equal(t,
		[]struct{ X string }{{X: "Joe"}, {X: "Ellen"}},
		expect)
}

func TestGetAllPrefix(t *testing.T) {
	d := MustNew(t.TempDir())
	// require.NoError(t, d.Init())

	require.NoError(t, d.Set("repositories", "parents/mom", struct{ X string }{X: "Ellen"}))
	require.NoError(t, d.Set("repositories", "parents/dad", struct{ X string }{X: "Joe"}))
	require.NoError(t, d.Set("repositories", "kids/son", struct{ X string }{X: "James"}))

	var expect []struct{ X string }
	require.NoError(t, d.GetAll("repositories", "parents/", &expect))
	require.Equal(t,
		[]struct{ X string }{{X: "Joe"}, {X: "Ellen"}},
		expect)
}
