package prism

import (
	"encoding/json"
	"errors"
	"fmt"
	"log/slog"
	"path"
	"reflect"

	"gitlab.oit.duke.edu/devil-ops/packageprism/db"
	"gitlab.oit.duke.edu/devil-ops/packageprism/indexers"
)

// Repo is a single tree that can serve up packages of a given type
type Repo struct {
	ID      string           `yaml:"id" json:"id"`
	Indexer indexers.Indexer `yaml:"indexer" json:"indexer"`
	// Properties PropertyMap      `yaml:"properties,omitempty" json:"properties,omitempty"`
	server *Server
}

// UnmarshalJSON implements a custom marshaller for json. We mainly need this to convert the 'type' to an interface
func (r *Repo) UnmarshalJSON(data []byte) error {
	// Ignore null, like in the main JSON package.
	if string(data) == "null" || string(data) == `""` {
		return nil
	}

	var target map[string]any
	if err := json.Unmarshal(data, &target); err != nil {
		return err
	}

	t, err := extractIndexer(target)
	if err != nil {
		return err
	}

	*r = Repo{
		ID:      trimQuotes(target["id"].(string)),
		Indexer: t,
	}

	return nil
}

func extractIndexer(m map[string]any) (indexers.Indexer, error) {
	// Do we have an "indexer field?"
	if _, ok := m["indexer"]; !ok {
		return nil, errors.New("missing 'indexer' field")
	}
	// Or is this the new fancy descriptive map?
	if indexerM, ok := m["indexer"].(map[string]any); ok {
		if ot, ok := indexerM["type"]; ok {
			return indexers.CreateWithMap(ot.(string), indexerM)
			/*
				switch ot.(string) {
				case indexers.APT:
					got, err := apt.NewWithMap(indexerM)
					if err != nil {
						return nil, err
					}
					return got, nil
				case indexers.YUM:
					got, err := yum.NewWithMap(indexerM)
					if err != nil {
						return nil, err
					}
					return got, nil
				case indexers.ASSET:
					got, err := asset.NewWithMap(indexerM)
					if err != nil {
						return nil, err
					}
					return got, nil
				case "":
					return nil, fmt.Errorf("empty type field in indexer")
				default:
					return nil, fmt.Errorf("unknown repo type while extracting map based indexer: %v", ot)
				}
			*/
		}
		return nil, fmt.Errorf("missing 'type' field in indexer")
	}
	return nil, fmt.Errorf("no idea how to extract from: %v", reflect.TypeOf(m["indexer"]))
}

// UnmarshalYAML does the right thing for repos when we need to unmarshal em
func (r *Repo) UnmarshalYAML(unmarshal func(interface{}) error) error {
	var target map[string]any
	if err := unmarshal(&target); err != nil {
		return err
	}
	var ok bool
	if r.ID, ok = target["id"].(string); !ok {
		return errors.New("missing 'id' field")
	}
	slog.Debug("attempting to unmarshal", "repo", target, "id", r.ID)
	t, err := extractIndexer(target)
	if err != nil {
		return err
	}
	*r = Repo{
		ID:      target["id"].(string),
		Indexer: t,
	}

	return nil
}

func (r Repo) dbPath() []byte {
	return []byte(path.Join(string(repoBucket), r.ID))
}

func (r Repo) fsPath() string {
	return path.Join(r.server.storagePath, r.ID)
}

// RepoList is multiple Repo entries
type RepoList []Repo

// Len returns the length of the item
func (r RepoList) Len() int {
	return len(r)
}

// Less returns true if i is less than j
func (r RepoList) Less(i, j int) bool {
	return r[i].ID < r[j].ID
}

// Swap swaps item i for item j
func (r RepoList) Swap(i, j int) {
	r[i], r[j] = r[j], r[i]
}

func (r *Repo) commit() error {
	if err := r.server.DB.Set(db.RepoBucket, r.ID, r); err != nil {
		return err
	}
	return nil
}
