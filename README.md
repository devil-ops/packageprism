# Package Prism

Host your artifacts and metadata through an http server

## Features

### RPM PackageS

Uses yum/dnf compatible metadata

### DEB Packages

Uses apt compatible metadata

### NuGet Packages

Coming soon

## Usage

### Running the server

```bash
$ prismctl serve -c prism.conf
...
```

Example Config:

```yaml
## This is where everything is stored. Must be a local path right now
storage_path: "/srv/prism"
## Use this as a bearer token for any admin operations, such as adding packages
admin_token: "test"
## Listen address
addr: 127.0.0.1:8090
## List of repositories to initialize at startup
repos:
  - id: "test-apt"
    indexer: "apt"
  - id: "test-yum"
    indexer: "yum"
```

### Uploading APT Packages

```bash
$ curl -X POST http://localhost:8090/api/v1/repos/test-apt/packages/ \
  --upload-file /home/drews-p/helloworld_1.0.0_amd64.deb \
  --header "Authorization: Bearer test"
```

### Configuring APT

You can use a source like below to connect to your APT repo.

Note that we are using the repo ID as both the base URL and the Distriobution name. This may change down the road

Copy the cert from the base of the repo on the filesystem (test-apt.asc in this case) to `/etc/apt/test-apt.asc`

```text
deb [arch=amd64 signed-by=/etc/apt/test.asc] http://localhost:8090/public/test test main
```
